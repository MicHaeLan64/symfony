<?php

namespace App\Controller;

use App\Service\BranchService;
use App\Service\ResponseService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Branch Controller
 *
 * @Route(
 *     path = "/branch"
 * )
 */
class BranchController extends Controller
{
	private $branchService;

	private $responseService;

	/**
	 * BranchController constructor.
	 *
	 * Service injection
	 *
	 * @param BranchService $branchService
	 */
	public function __construct(BranchService $branchService, ResponseService $responseService)
	{
		$this->branchService = $branchService;
		$this->responseService = $responseService;
	}

	/**
	 * List all branches info
	 *
	 * @Route(
	 *     path = "/all",
	 *     name = "branch_all",
	 *     methods = {"GET"}
	 * )
	 *
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function allAction()
	{
		// initialize response
		$response = $this->responseService->initialize();

		// fetch branch info and assign to response
		$branches = $this->branchService->all();
		$response = $this->responseService->success($branches);
		
		return $this->json($response);
	}
}
