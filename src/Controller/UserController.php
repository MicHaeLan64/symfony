<?php

namespace App\Controller;

use App\Service\BranchService;
use App\Service\LoginLogService;
use App\Service\ResponseService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * User Controller
 *
 * @Route(
 *     path = "/user"
 * )
 */
class UserController extends Controller
{
	private $responseService;

	private $userService;

	private $branchService;

	private  $loginLogService;

	/**
	 * UserController constructor.
	 *
	 * Service injection
	 *
	 * @param UserService $userService
	 * @param BranchService $branchService
	 * @param LoginLogService $loginLogService
	 */
	public function __construct(ResponseService $responseService, UserService $userService, BranchService $branchService, LoginLogService $loginLogService)
	{
		// inject service
		$this->responseService = $responseService;
		$this->userService = $userService;
		$this->branchService = $branchService;
		$this->loginLogService = $loginLogService;
	}

	/**
	 * @Route(
	 *     path = "/",
	 *     name = "user_list",
	 *     methods = {"GET"}
	 * )
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function allAction()
	{
		// initialize response
		$response = $this->responseService->initialize();

		// get user list
		$users = $this->userService->all();

		// send success response with user data
		$response = $this->responseService->success($users);
		return $this->json($response);
	}

	/**
	 * User login api
	 *
	 * @Route(
	 *     path = "/login",
	 *     name = "user_login",
	 *     methods = {"POST", "OPTIONS"}
	 * )
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
    public function loginAction(Request $request)
    {
		// initialize response
		$response = $this->responseService->initialize();

		// get post and options request params
    	$name = $request->request->get('name') ? $request->request->get('name') : $request->query->get('name');
		$pwd = $request->request->get('pwd') ? $request->request->get('pwd') : $request->query->get('pwd');
		$branch = $request->request->get('branch') ? $request->request->get('branch') : $request->query->get('branch');
		if (!empty($name) && !empty($pwd) && !empty($branch)) {
			// validate user and branch
			$userValidation = $this->userService->loginValidation($name, $pwd);
			$branchValidation = $this->branchService->exist($branch);

			// get user info and register login action if validate
			if ($userValidation && $branchValidation) {
				// get user info
				$user = $this->userService->findByName($name);

				// register login action
				$this->loginLogService->login($user->getId(), $branch);

				// set response success
				$data = array(
					'id' => $user->getId(),
					'name' => $user->getName(),
					'locale' => $user->getLocale(),
					'permission' => $user->getPermission()
				);
				$response = $this->responseService->success($data);
			}
		}

        return $this->json($response);
    }
}
