<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180430094551 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE branch (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) NOT NULL, ADD locale VARCHAR(255) NOT NULL, ADD enabled VARCHAR(1) NOT NULL, DROP uname, DROP pkey, CHANGE pwd pwd VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE branch');
        $this->addSql('ALTER TABLE user ADD uname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD pkey VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP name, DROP locale, DROP enabled, CHANGE pwd pwd VARCHAR(32) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
