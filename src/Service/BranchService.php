<?php

namespace App\Service;

use App\Repository\BranchRepository;

class BranchService
{
	private $branchRepository;

	/**
	 * Inject branch repository
	 * BranchService constructor.
	 * @param BranchRepository $branchRepository
	 */
	public function __construct(BranchRepository $branchRepository)
	{
		$this->branchRepository = $branchRepository;
	}

	/**
	 * Check if branch exist
	 * @param $id
	 * @return bool
	 */
	public function exist($id)
	{
		$exist = FALSE;

		$branch = $this->branchRepository
			->find($id);

		if (!empty($branch)) {
			$exist = TRUE;
		}

		return $exist;
	}

	/**
	 * Get all branches info
	 * @return \App\Entity\Branch[]
	 */
	public function all()
	{
		$branches = $this->branchRepository
			->findAll();

		return $branches;
	}
}