<?php

namespace App\Service;

use App\Entity\LoginLog;
use App\Repository\LoginLogRepository;
use Doctrine\ORM\EntityManagerInterface;

class LoginLogService
{
	const LOGIN = 1;
	const LOGOUT = 2;

	private $entityManager;

	private $loginLogRepository;

	/**
	 * LoginLogService constructor.
	 * @param EntityManagerInterface $entityManager
	 * @param LoginLogRepository $loginLogRepository
	 */
	public function __construct(EntityManagerInterface $entityManager, LoginLogRepository $loginLogRepository)
	{
		$this->entityManager = $entityManager;
		$this->loginLogRepository = $loginLogRepository;
	}

	/**
	 * Create login record
	 * @param $uid
	 * @param $bid
	 */
	public function login($uid, $bid)
	{
		// find last record
		$lastRecord = $this->last($uid);

		// create login record
		if ($lastRecord->getAction() == self::LOGOUT) {
			$this->createRecord($uid, $bid, self::LOGIN);
		} elseif ($lastRecord->getBid() != $bid) {
			// create logout record of last branch
			$this->createRecord($uid, $lastRecord->getBid(), self::LOGOUT);

			// create login record
			$this->createRecord($uid, $bid, self::LOGIN);
		} elseif ($lastRecord->getDatetime()->format('Ymd') != date('Ymd')) {
			$this->createRecord($uid, $bid, self::LOGIN);
		}
	}

	/**
	 * Create log record
	 * @param $uid
	 * @param $bid
	 * @param $action
	 */
	private function createRecord($uid, $bid, $action)
	{
		// create record
		$log = new LoginLog();
		$log->setUid($uid)
			->setBid($bid)
			->setDatetime(new \DateTime())
			->setAction($action);

		// save record
		$this->entityManager
			->persist($log);
		$this->entityManager
			->flush();
	}

	private function last($uid)
	{
		$record = $this->loginLogRepository
			->findOneBy(
				array(
				'uid' => $uid
				),
				array(
					'id' => 'DESC'
				)
			);

		return $record;
	}
}