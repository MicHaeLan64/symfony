<?php

namespace App\Service;


class ResponseService
{
	private $codeError;

	private $codeSuccess;

	public function __construct($codeError, $codeSuccess)
	{
		$this->codeError = $codeError;
		$this->codeSuccess = $codeSuccess;
	}

	public function initialize()
	{
		return array(
			'code' => $this->codeError,
			'data' => array()
		);
	}

	public function success($data)
	{
		return array(
			'code' => $this->codeSuccess,
			'data' => $data
		);
	}
}