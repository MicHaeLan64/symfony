<?php

namespace App\Service;

use App\Repository\UserRepository;

class UserService
{
	// set const
	const ENABLED = 1;
	const DISABLED = 2;

	private $userRepository;

	/**
	 * Inject user repository
	 * UserService constructor.
	 * @param UserRepository $userRepository
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	/**
	 * Validate login by checking user exist and password match
	 * @param $name
	 * @param $pwd
	 * @return bool
	 */
	public function loginValidation($name, $pwd)
	{
		$check = FALSE;

		$nameCheck = $this->nameCheck($name);
		$pwdCheck = $this->pwdCheck($name, $pwd);

		if ($nameCheck && $pwdCheck) {
			$check = TRUE;
		}

		return $check;
	}

	/**
	 * Check if user exist
	 * @param $name
	 * @return bool
	 */
	private function nameCheck($name)
	{
		$exist = FALSE;

		$user = $this->userRepository
			->findOneBy(
				array(
					'name' => $name,
					'enabled' => self::ENABLED
				)
			);
		if (!empty($user)) {
			$exist = TRUE;
		}

		return $exist;
	}

	/**
	 * Check if username and password match
	 * @param $name
	 * @param $pwd
	 * @return bool
	 */
	private function pwdCheck($name, $pwd)
	{
		$match = FALSE;

		$user = $this->userRepository
			->findOneBy(
				array(
					'name' => $name,
					'pwd' => md5($pwd),
					'enabled' => self::ENABLED
				)
			);

		if (!empty($user)) {
			$match = TRUE;
		}

		return $match;
	}

	/**
	 * Get user by username
	 * @param $name
	 * @return \App\Entity\User|null
	 */
	public function findByName($name)
	{
		$user = $this->userRepository->findOneBy(array('name' => $name));

		return $user;
	}

	/**
	 * Get user list
	 * @return \App\Entity\User[]
	 */
	public function all()
	{
		return $this->userRepository->findAll();
	}
}