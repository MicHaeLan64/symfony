<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\UserController' shared autowired service.

include_once $this->targetDirs[3].'/src/Service/UserService.php';
include_once $this->targetDirs[3].'/src/Service/LoginLogService.php';
include_once $this->targetDirs[3].'/vendor/symfony/dependency-injection/ContainerAwareInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/dependency-injection/ContainerAwareTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/ControllerTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/Controller.php';
include_once $this->targetDirs[3].'/src/Controller/UserController.php';
include_once $this->targetDirs[3].'/src/Service/ResponseService.php';

return $this->services['App\Controller\UserController'] = new \App\Controller\UserController(($this->privates['App\Service\ResponseService'] ?? $this->privates['App\Service\ResponseService'] = new \App\Service\ResponseService(1, 0)), new \App\Service\UserService(($this->privates['App\Repository\UserRepository'] ?? $this->load('getUserRepositoryService.php'))), ($this->privates['App\Service\BranchService'] ?? $this->load('getBranchServiceService.php')), new \App\Service\LoginLogService(($this->services['doctrine.orm.default_entity_manager'] ?? $this->load('getDoctrine_Orm_DefaultEntityManagerService.php')), ($this->privates['App\Repository\LoginLogRepository'] ?? $this->load('getLoginLogRepositoryService.php'))));
