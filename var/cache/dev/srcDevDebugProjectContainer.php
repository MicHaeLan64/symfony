<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerUxo1sxc\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerUxo1sxc/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerUxo1sxc.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerUxo1sxc\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \ContainerUxo1sxc\srcDevDebugProjectContainer(array(
    'container.build_hash' => 'Uxo1sxc',
    'container.build_id' => 'd4c9f0f9',
    'container.build_time' => 1525340588,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerUxo1sxc');
