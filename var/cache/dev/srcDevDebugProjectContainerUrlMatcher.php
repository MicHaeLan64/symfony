<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // branch_all
        if ('/branch/all' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\BranchController::allAction',  '_route' => 'branch_all',);
            if (!in_array($canonicalMethod, array('GET'))) {
                $allow = array_merge($allow, array('GET'));
                goto not_branch_all;
            }

            return $ret;
        }
        not_branch_all:

        if (0 === strpos($pathinfo, '/user')) {
            // user_list
            if ('/user' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\UserController::allAction',  '_route' => 'user_list',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_user_list;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'user_list'));
                }

                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_user_list;
                }

                return $ret;
            }
            not_user_list:

            // user_login
            if ('/user/login' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\UserController::loginAction',  '_route' => 'user_login',);
                if (!in_array($requestMethod, array('POST', 'OPTIONS'))) {
                    $allow = array_merge($allow, array('POST', 'OPTIONS'));
                    goto not_user_login;
                }

                return $ret;
            }
            not_user_login:

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
